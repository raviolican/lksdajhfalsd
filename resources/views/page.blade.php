@extends('layouts.app')
@section('header')
    <title>{{$pPage->title}}</title>
    <meta name="description" content="{{$pPage->description}}">
@endsection
@section('content')
    <div class="container">
        {!! $pPage->html !!}
    </div>
@endsection