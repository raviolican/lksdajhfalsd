<div class="col-md-12 footerCol" style="padding: 0px !important;">
    <div class="footer" style="background-color: #111111 !important;">
        <div class="container">
            <div class="clearfix">
                <div class="footer-logo">
                    <a href="#">{{$settings["title"]}}</a>
                </div>
                <dl class="footer-nav">
                    <dt class="nav-title"></dt>
                    <dd class="nav-item"><a href="#">Privacy Policy</a></dd>
                    <dd class="nav-item"><a href="#">Impressum</a></dd>
                    <dd class="nav-item"><a href="#">Contact Us</a></dd>
                </dl>
            </div>
            <div class="footer-copyright text-center">
                {{$settings["copyright"]}} <br>
                <?= "<small style=\"font-size: xx-small\">  Dsigned by Simon Daum in Austria</small>" ?>
            </div>
        </div>
    </div>
</div>