<div class="container col-md-3 adm-nav-div" style="margin-left: -10px">
    <ul class="list-group sidebar-nav-v1  adm-nav-div" id="sidebar-nav">
        <!-- Site Settings -->
        <li class="list-group-item adm-nav-li">
            <a class="adm-nav-section-header">Site Settings</a>
            <ul >
                <li><a href="{{url("admin/site/meta")}}">Meta Configurations</a></li>
                <li><a href="{{url("admin/google")}}">Google</a></li>
                <li><a href="{{url("admin/amazon")}}">Amazon API</a></li>
                <li><a href="{{url("admin/slides")}}">Slider</a></li>
            </ul>
        </li>
        <!-- Site Settings -->

        <!-- Amazon Settings -->
        <li class="list-group-item adm-nav-li">
            <a class="adm-nav-section-header">Amazon</a>
            <ul >
                <li><a href="{{url("admin/products/view")}}">View Products</a></li>
                <li><a href="{{url("admin/products/add")}}">Add Product</a></li>
                <li><a href="{{url("admin/products/categories")}}">Manage Categories</a></li>
            </ul>
        </li>
        <!-- Site Settings -->

        <!-- Site Settings -->
        <li class="list-group-item adm-nav-li">
            <a class="adm-nav-section-header">Blog</a>
            <ul>
                <li><a href="{{url("admin/blog/view")}}">View Articles</a></li>
                <li><a href="{{url("admin/blog/add")}}">New Article</a></li>
            </ul>
        </li>

        <!-- Site Settings -->
        <li class="list-group-item adm-nav-li">
            <a class="adm-nav-section-header" >Custom Pages</a>
            <ul>
                @foreach($pages as $page)
                    <li><a href="{{url("admin/pages/".$page->id)}}">{{$page->title}}</a></li>
                @endforeach
                <li><a href="{{url("admin/pages/new")}}">+ Create a new Page</a></li>
            </ul>
        </li>
    </ul>
</div>