@extends('layouts.app')
@section('header')
    <script src="{{asset("js/tinymce/tinymce.min.js")}}"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection
@section('footer')
    <script src="{{asset("js/codeeditor/codemirror.js")}}"></script>
    <link rel="stylesheet" href="{{asset("css/codeeditor/codemirror.css")}}">
    <script src="{{asset("js/codeeditor/mode/javascript/javascript.js")}}"></script>
    <script type="text/javascript">
        var area = document.getElementById('additional_js');
        var myCodeMirror = CodeMirror.fromTextArea(area, {
            lineNumbers: true
        });
    </script>
@endsection

@section('content')

    <div class="containter">
        @include("layouts.adminNavigation")

        <div class="col-md-7">
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(\Illuminate\Support\Facades\Session::has("notification"))
                <div class="alert alert-info fade in">
                    {{(\Illuminate\Support\Facades\Session::get("notification"))}}
                </div>
            @endif
            <form action="./add" method="POST"  enctype="multipart/form-data" >
                {{csrf_field()}}
                <h1>Add a new Blogpost</h1>
                <fieldset>
                    <section>
                        <label for="title">Titel</label>
                        <input class="form-control" type="text" id="title" name="title" id="title" value="">
                    </section>

                    <section>
                        <label for="seoslug">Seo-Slug</label>
                        <input class="form-control" type="text" id="seoslug" name="seo_slug" id="slug" value="">
                    </section>

                    <section>
                        <label for="desc">Description</label>
                        <input class="form-control" type="text" id="desc"
                               name="description" placeholder="about 166 characters long" >
                    </section>

                    <section>
                        <label for="image">Post-Image</label>
                        <input   type="file" name="image" id="image">
                    </section>
                    <section>
                        <label for="image_org">Image Organization</label>
                        <input class="form-control" type="text" id="image_org" name="image_organization" placeholder="" >
                    </section>
                    <section>
                        <label for="image_credit">Image Credit</label>
                        <input class="form-control" type="text" name="image_credit" id="image_credit" placeholder="" >
                    </section>
                    <section>
                        <label for="image_cap">Image Caption</label>
                        <input  class="form-control" type="text" id="image_cap" name="image_caption" placeholder="" >
                    </section>

                    <section>
                        <label for="article_txt">Article</label>
                        <textarea class="form-control" id="article_txt" rows="10" id="article" name="text"></textarea>
                    </section>

                    <section>
                        <label for="kwds">Keywords</label>
                        <input class="form-control" type="text" id="kwds" name="keywords" >
                    </section>

                    <section>
                        <label for="cats">Category</label>
                        <select name="category" id="cats" class="form-control">
                            <option value="news">News</option>
                            <option value="uhren_test">Uhren-Test</option>
                        </select>
                    </section>
                </fieldset>
                <br>
                <footer>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-primary"
                            onclick="window.history.back();">Back</button>
                </footer>
            </form>
        </div>

    </div>
@endsection