@extends('layouts.app')
@section('footer')
    <script src="{{asset("js/codeeditor/codemirror.js")}}"></script>
    <link rel="stylesheet" href="{{asset("css/codeeditor/codemirror.css")}}">
    <script src="{{asset("js/codeeditor/mode/javascript/javascript.js")}}"></script>
    <script type="text/javascript">
        var area = document.getElementById('additional_js');
        var myCodeMirror = CodeMirror.fromTextArea(area, {
            lineNumbers: true
        });
    </script>
@endsection

@section('content')
    <div class="containter">
        @include("layouts.adminNavigation")

        <div class="col-md-7">
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <h2> Amazon Settings </h2>
            <p>
                Configure Amazon access and features.
            </p>
            <form action="/admin/site/meta" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="a_c_key_id">Access Key ID</label>
                    <input type="text" name="a_c_key_id" value="{{$settings["a_c_key_id"]}}" class="form-control"
                              id="inputTitle" aria-describedby="a_c_key_idHelp"/>
                    <small id="a_c_key_idHelp" class="form-text text-muted"> </small>
                </div>

                <div class="form-group">
                    <label for="a_c_key_secret">Secret Access Key</label>
                    <input type="text" name="a_c_key_secret" value="{{$settings["a_c_key_secret"]}}" class="form-control"
                              id="inputTitle" aria-describedby="a_c_key_secretHelp"/>
                    <small id="a_c_key_secretHelp" class="form-text text-muted">Find those 2 in <i>My Security Credentials</i> in your AWS IAM Console.</small>
                </div>

                <div class="form-group">
                    <label for="a_associateTag">Associate Tag</label>
                    <input type="text" name="a_associateTag" value="{{$settings["a_associateTag"]}}" class="form-control"
                           id="inputTitle" aria-describedby="a_associateTagHelp"/>
                    <small id="a_associateTagHelp" class="form-text text-muted">You Amazon associate tag.</small>
                </div>

                <div class="form-group">
                    <label for="a_country">Country</label>
                    <input type="text" name="a_country" value="{{$settings["a_country"]}}" class="form-control"
                           id="inputTitle" aria-describedby="a_countryHelp"/>
                    <small id="a_countryHelp" class="form-text text-muted">Choose your prefered country. <br><a
                                href="http://docs.aws.amazon.com/AWSECommerceService/latest/DG/localevalues.html" target="_blank">List of locales</a></small>
                </div>

                <section>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </section>
            </form>

        </div>

    </div>
@endsection