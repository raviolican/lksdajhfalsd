

<?php
/*
 * Using modified layout for optimizing conversions!
 */
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <ol class="breadcrumb" style="" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a href="{{url("/")}}" itemscope itemtype="http://schema.org/Thing" itemprop="item">
                    <span itemprop="name">Home</span>
                </a>
                <meta itemprop="position" content="1" />
            </li>

            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a href="{{url("/blog")}}" itemscope itemtype="http://schema.org/Thing" itemprop="item" >
                    <span itemprop="name">Blog</span>
                </a>
                <meta itemprop="position" content="2" />
            </li>
        </ol>

            <div class="col-md-9">
                <br>
            @foreach($articles as $article)
                <div class="media">
                    <a class="pull-left"  href="{{url("/blog/view/".$article->id."/".$article->seo_slug)}}">
                        <img class="media-object" src="{{url($article->article_image)}}">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$article->title}}</h4>
                        <p class="text-right">By Admin</p>
                        <p>{{$article->description}}</p>
                        <ul class="list-inline list-unstyled">
                            <li><span><i class="glyphicon glyphicon-calendar"></i>{{$article->created_at->diffForHumans()}} </span></li>


                        </ul>
                    </div>
                </div>
                    <br>
            @endforeach

                <div class="text-center">
                    {!! $articles->render() !!}
                </div>

            </div>
            <!-- Right Sidebar -->
            <div class="col-md-3">
                <!-- Social Icons -->
                <div>
                    <h5>Follow Us</h5>
                    <p>
                        TWITTER | FACEBOOK
                    </p>

                </div>
                <div >
                     <h5>About Us</h5>
                    <p>
                        Uhren123 ist spezialisert auf Uhren, wir bieten günstige Preise mit bester Qualität bei schneller Lieferung
                    </p>
                </div>
                <!-- End Tabs Widget -->


            </div>
            <!-- End Right Sidebar -->

        </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            ajaxuse = false;


            $(".qtyChekc").on("change", function () {

                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/" + $(this).attr("asin") + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });

            $(".close").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/delete/')}}' + "/" + $(this).attr("asin"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;

                }

            });
        });

    </script>
@endsection