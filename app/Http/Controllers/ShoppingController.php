<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Site;
use App\Slider;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ShoppingController
 * @package App\Http\Controllers
 */
class ShoppingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /* Fast Caching. This is just WoW!*/
        $slides = Cache::remember("slides", 240, function() {
            return  Slider::where("active", "=", "1")->get();
        });
        $featuredProducts = Cache::remember("featured", 240, function () {
            return  Product::where('featured', 1)->get()->all();
        });
        $latestProducts = Cache::remember("products", 240, function() {
            return  Product::orderBy('created_at', 'asc')->take(10)->get();
        });
        return view('welcome', compact('featuredProducts'), compact('latestProducts', 'slides'));
    }

    /**
     * @param bool $variations
     * @return bool
     */
    static function validateVariations($variations = FALSE){
        return $variations;
    }

    /**
     * Update the PRICE in DB!
     * @param $product
     * @param $price
     * @return
     */
    static function updatePrice($product, $price){
        if($product->price != $price) {
            $product->price = $price;
            $product->increment("price_updates");
            $product->save();
            return true;
        } else {
            return;
        }
    }

    /**
     * @param Request $request
     * @param $productId
     * @return $this|string
     */
    public function quickReview($productId)
    {
        return \Cache::remember($productId, 0, function() use ($productId)
        {
            $j = (Product::where('id', $productId)->select("features")->get()->first()["features"]);
            if($j[1] == "}")
                return '{ "0":"Features nicht verfügbar" }';
            return (Product::where('id', $productId)->select("features")->get()->first()["features"]);
        });
    }

    /**
     * @return bool
     */
    static function getLatestViewedProducts()
    {

        $viewArray = array();

        if(session()->get("viewed_products") === NULL)
        {
            return FALSE;
        }
        foreach(session()->get("viewed_products") as $key => $value )
        {
            $viewArray[] = $value;
        }

        // Fetch some other products ....
        $products = \DB::table("products")->select("name","color","id", "price",
            "thumbnail", "seo_slug")
            ->where(function ($query) use ($viewArray)
            {
                foreach ($viewArray AS $asin)
                {
                    $query->orWhere("asin", $asin);
                }
            })->take(10)->get();
        // Fetch some other products ....

        /*
        // Maybe make duplicates if there are not so many products!
        $products += \DB::table("products")->select("name","id", "price", "thumbnail", "seo_slug")
            ->where(function ($query) use ($products)
            {
                foreach ($products AS $relation)
                {
                    $query->orWhere("asin", 'LIKE', $relation->name);
                    $query->orWhere("color", 'LIKE', $relation->color);
                }
            })
            ->take(10)->get();
        */
        return $products;
    }

    /**
     * @param Request $request
     * @param $gender
     * @return string
     */
    public function renderCategoryPage(Request $request, $mainCategory, $cat = NULL, $brand = NULL)
    {

        // Generating the ASIN array from SESSION
        // Check if the visitor is _NOT_ looking for human wearables
        if($cat !== NULL) {
            $search = $this->compileSearchField($mainCategory,$cat);
        } else {
            // No specific category set?
            // Lets make that it loads ALL
            $cat = FALSE;
            $search = $this->compileSearchField($mainCategory);
        }

        // Brands
        if(!isset($request->brands))
            $brands = $search["brands"];
        else
            if($brand !== NULL)
                $brands = $brand;
            else
                $brands = $request->brands;

        // Checking if the user had choosen a custom number of products per page ...
        if($request->session()->has("show")) {


            // Did he want to see ALL products?
            if ($request->session()->get("show") !== "all") {

                $showPaginate = $request->session()->get("show");
            } else {
                $showPaginate = 30;
            } // Limit it to 30.

        } else {
            // .. Everything normal...
            $showPaginate = 12;
        }


        // Checking for sorting...
        if($request->session()->has("sort_by"))
        {
            //$sortBy = $request->session()->get("sort_by");
            $sortBy = explode(" ",  $request->session()->get("sort_by"));
        } else {
            // .. Everything normal...
            $sortBy =  array("views","asc");
        }
        // Get Recommendet products
        $recommendations = $this->getLatestViewedProducts();
        $products =  $this->getProducts($showPaginate,$sortBy,$mainCategory,
            $brands, $cat, $request->colors, $request->price);

        if(count($products) == 0)
            throw new NotFoundHttpException();
        $request->flash();

        return view("category", compact('search', 'products','recommendations'))->withInput($request);

    }

    /**
     * @param array $gender
     * @param array $brand
     * @param array $category
     * @param array $color
     * @return mixed
     */
    public function getProducts($showPaginate, $sortBy, $gender = array(), $brand = array() ,
                                $category = array(),$color = array(),$price = array())
    {

        // Check if sub-category has been accessed
        if($category != false)
            $gender = $category;
        // Get the Category
        $myC = Category::where("name", ucfirst($gender) )->get();

        // Build Query
        $products = Category::find($myC[0]->id)->products()
            // Brand Filter
            ->where(function ($query) use ($brand) {
                if(count($brand) === 1)
                    $query->orWhere('brand', 'LIKE', $brand);
                else if( count($brand) > 1 )
                    foreach ($brand as $b)
                        $query->orWhere('brand', 'LIKE', $b);
                else
                    return;
            })
            // Color Filter
            ->where(function ($query) use ($color) {
                If(!isset($color))
                    return;
                foreach ($color as $c)
                    $query->orWhere('color', 'LIKE', $c);

                $query->orWhere('color', '');
            })
            // Price Filter
            ->where(function ($query) use ($price) {
                if(isset($price["lower"]) && isset($price["upper"]))
                    $query->whereBetween('price', array(str_replace(".",
                        "",$price["lower"]),str_replace(".", "",$price["upper"])));
                else
                    return;
            })
            ->orderBy($sortBy[0], $sortBy[1])
            ->paginate($showPaginate);

        // Nothing found? 404!
        return $products;
    }

    /**
     * Creates and propagates an array structure that will be used by view to create a search
     * fild (sidebar of category page
     *
     * @param $gender the main category
     * @param $category sub category of main category
     * @return bool false on error
     * @return array compiled search array structure
     */
    public function compileSearchField($gender, $category = null)
    {
        $search = array(
            "gender" => $gender, "brands" => array(), "color" => array(), "categories" => array()
        );

        // Checking if sub category is given
        if(is_null($category)){
            // Nope
            $products = Category::with("products")->where("name", $gender)->get();
            $products = $products[0]->products;
        }
        else{
            // Yes It's here ! So look for category too!
            $products = Category::with("products")->where("name", $category)->get();
            $products = $products[0]->products;
        }

        // No products were found! (Will show a 404 message)
        if(count($products) === 0)
            return false;
        // Max & Minprices!
        $search["minprice"] = $products[0]->minprice;
        $search["maxprice"] = $products[0]->maxprice;

        // Fill the searchfield arrays!
        foreach($products as $product)
        {
            if(!isset($search["colors"][ucfirst(strtolower($product->color))]) && $product->color != NULL) {
                $s = ucfirst(strtolower($product->color));
                $search["colors"][$s] = $s;
            }
            if(!isset($search["brands"][ucfirst(strtolower($product->brand))])) {
                $s = ucfirst(strtolower($product->brand));
                $search["brands"][$s] = $s;
            }
            if(!isset( $search["categories"][ucfirst(strtolower($product->category))] )) {
                // Check if main category was requested
                if($gender ==$product->categories[0]->name )
                    continue;
                $s = ucfirst(strtolower($product->categories[0]->name));
                $search["categories"][$s] = $s;
            }
        }
        // Bye!
        return $search;
    }

    /**
     * Updateing the visitors session settings!
     * @param Request $request
     * @param $number
     */
    public function editShowNumber(Request $request, $number)
    {
        // Validate
        if(!in_array($number,array("6","15","18","all")))
            return;
        else
            $request->session()->put("show", $number);
    }

    /**
     * @param Request $request
     * @param $sort
     */
    public function editSortBy(Request $request, $sort)
    {
        // Validate ..
        if(!in_array($sort,array("price desc","price asc","popularity desc","popularity asc"))) {
            return;
        } else {
            $request->session()->put("sort_by", $sort);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderPage($id)
    {
        $pPage = Site::find($id);
        return view("page", compact("pPage"));

    }
}
