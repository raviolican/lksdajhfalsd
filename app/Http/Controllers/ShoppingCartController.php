<?php

namespace App\Http\Controllers;

/**
 * This file basically contains important functions that allows to add / modify or delete products from the cart
 * stored locally and stored on Amazon Cart.
 * Attention: The functionality can break completely on small changes, so beware on doing modifications here.
 *
 * Todo: Maybe add constructor which creates a Amazon object
 */
use App\Http\Controllers\Amazon\Amazon;
use App\Product;
use Illuminate\Http\Request;
use Symfony\Component\Translation\Exception\InvalidResourceException;

class ShoppingCartController extends Controller
{
    /**
     * Adds functionallity for a) Create a Amazon Cart and assign first product. b) Add a new product to the Cart and
     * c) update a product's quantity
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addProductToCart(Request $request)
    {
        $amazon = new Amazon();
        $session_cart = $request->session()->get("shopping_cart");

        // Please check if a Cart has already been created!
        if (!$request->session()->has("amz_cart")) {
            // Not created yet. So lets create a new cart!
            try {
                $xml = $amazon->createCart($request->asin, ($request->qty == "undefined" ? 1 : $request->qty));

            } catch (InvalidResourceException $e) {
                echo "Sorry, this product is currently not available.";
                return;
            }

            // Store everything important in the user's session.
            $request->session()->put("amz_cart", (string)$xml->Cart->CartId[0]);
            $request->session()->put("amz_hmac", (string)$xml->Cart->HMAC[0]);
            $request->session()->put("amz_pruchase_url", (string)$xml->Cart->PurchaseURL[0]);

            // Here we add the product to our local cart. Session based.
            SessionController::addProductToCard(
                $request,
                $request->asin,
                (integer) ($request->qty == "undefined" ? 1 : $request->qty),
                $xml->Cart->CartItems->CartItem[0]->CartItemId
            );
            echo "Product added to your cart!";

        } else {
            // Cart already created.
            if (isset($session_cart[$request->asin])) { // Check if the request is for a product that already is on the
                                                        // user's cart.
                try {
                    $amazon->modifyCart_updateQty(
                        $session_cart[$request->asin]["CartItemId"],
                        $request->session()->get("amz_cart"),
                        $request->session()->get("amz_hmac"),
                        (integer)($request->qty == "undefined" ? 1 : $request->qty)
                    );
                    // Update Cart
                    SessionController::addProductToCard(
                        $request,
                        $request->asin,
                        $request->qty,
                        $session_cart[$request->asin]["CartItemId"]
                    );

                } catch (InvalidResourceException $e) {
                    echo "Sorry, this product is currently not available.";
                    return;
                }
                echo "Product added to your cart!";
            } else { // The product was not found on the cart, so here we going to add the new product to user's cart.
                try {
                    $response = $amazon->modifyCart_addCart(
                        $request->session()->get("amz_cart"),
                        $request->session()->get("amz_hmac"),
                        $request->asin,
                        (integer)($request->qty == "undefined" ? 1 : $request->qty)
                    );
                    // Update Cart
                    SessionController::addProductToCard(
                        $request,
                        $request->asin,
                        (integer)($request->qty == "undefined" ? 1 : $request->qty),
                        $response->Cart->CartItems->CartItem[0]->CartItemId[0]);
                } catch (InvalidResourceException $e) {
                    echo "Sorry, this product is currently not available.";
                    return;
                }
                echo "Product added to your cart!";
            }
            // ~ Fin ~
        }

    }
    /**
     * Get cart-added products
     * @return mixed
     */
    static function getCartProducts()
    {
        $asins = \Request::session()->get("shopping_cart");
        $products = Product::where(function ($query) use($asins)
        {
            if(count($asins)>=1  ){
                foreach ($asins as $asin => $qty) {
                    $query->orWhere("asin", $asin );
                }
            }
            else if(count($asins) != 0)
                $query->Where("asin", $asins);
            else
                return;
        });
        if(count($asins) == 0)
            return;
        else
            return $products->get() ;
    }

    /**
     * @param Request $request
     */
    public function deleteProductFromCart(Request $request)
    {
        /* Dirty */
        $amazon = new Amazon();
        $session_cart = $request->session()->get("shopping_cart");

        $response = $amazon->modifyCart_updateQty(
            $session_cart[$request->asin]["CartItemId"],
            $request->session()->get("amz_cart"),
            $request->session()->get("amz_hmac"),
            0 /* Setting qty to 0 will remove the productg from the amazon cart. */
        );
        SessionController::deleteCartItem($request,$request->asin);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderShoppingCart(Request $request)
    {
        $products = static::getCartProducts();
        return view("cart",compact("products"));
    }
}
