<?php

namespace App\Http\Controllers;

/**
 * Implements function to communicate with user's session. Especially cart functions are implemented
 * here.
 */
use Illuminate\Http\Request;

class SessionController extends Controller
{
    /**
     * If a user visits a specific product, this function is called and adds the product to the
     * viewed products array located in user's session.
     * @param $request
     * @param $viewed
     */
    static function viewedProducts($request, $viewed)
    {
        // lets see if the session already has the viewed product array
        if (session()->has("viewed_products"))
        {
            $visited = session()->get("viewed_products");
            // Is it already in the array ?
            if(in_array($viewed, $visited))
            {
                return;
            } else {
                // NO, so lets add it!
                $visited[] = $viewed;
                $visited = array_slice($visited,-15);
                $request->session()->forget('viewed_products');
                $request->session()->put("viewed_products", $visited);
            }
        } else {
            // No.. lets create it!
            $request->session()->push("viewed_products", $viewed);
        }
    }

    /**
     * Adds a new product to the cart
     * @param $request
     * @param $product
     * @param $qty
     * @param $CartItemId
     */
    static function addProductToCard($request, $product, $qty, $CartItemId)
    {
        if($request->session()->has("shopping_cart"))
        {
            $products = $request->session()->get("shopping_cart");
            // Is the ASIN in the array?
            if(array_key_exists($product,$products))
                $products[$product] =  [
                    "qty" => (string)$qty,
                    "CartItemId" => (string)$CartItemId
                ];
            else
                $products[$product] =  [
                    "qty" => (string)$qty,
                    "CartItemId" => (string)$CartItemId
                ];
            $request->session()->forget("shopping_cart");
            $request->session()->put("shopping_cart", $products);
        } else {
            $request->session()->put("shopping_cart.".$product, [
                "qty"=>$qty,
                "CartItemId" => (string)$CartItemId
            ]);
        }
    }

    /**
     * Removes product from the cart
     * @param $asin
     */
    static function deleteCartItem(Request $request, $asin)
    {
        $request->session()->forget("shopping_cart.".$asin);
    }
}
