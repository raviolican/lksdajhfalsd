<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');
            $table->text('features')->defaul("{}");
            $table->text('editorialreview')->default(NULL);
            $table->integer('featured')->default(0);
            $table->integer('salesRank')->default(0);
            $table->string('ItemAttributes',2000)->default(0);

            $table->string("name")->index();
            $table->string("brand")->index();
            $table->string("color")->index()->nullable();
            $table->integer("price")->index();
            $table->string("imgurls",2000);
            $table->string("thumbnail");
            $table->string("seo_slug")->index();
            $table->string("ean");

            $table->string("asin");
            $table->string("specs")->default("{}");

            $table->integer("views")->default("0");
            $table->integer("cardadded")->default("0");
            $table->float("bestoffer")->default("0");

            $table->integer("price_updates")->default("0");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
